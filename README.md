# Vector Racer Project
## Get started
### How to build
Run `cmake ../..` in the `trunk/build` directory. Then run `make` to build the whole application, and `make doc` to use *Doxygen* auto-generated documentation.
- `build/bin` contains binaries compiled from 'trunk/application',
- `build/doc` contains the doc.

### How to organize the project
- The sources files under 'trunk/src' uses headers in `trunk/include` and represents a library (e.g. *vectorMap*).
- The `trunk/examples` directory contains static assets (e.g. maps' files).
- The `trunk/application` directory contains source files of the application (i.e. files with a `main` function), and uses libraries built from `trunk/src`.
- The `trunk/doc` directory is used for *Doxygen* configuration.

All is configured with CMakeLists.txt. If you add a `library/application/file`, you'll need to edit them !

## Libraries summary
* vectorMap: *and a short explanation...*
* vectorStep: *and a short explanation...*
* vectorDll: *and a short explanation...*
* vectorSdl: *and a short explanation...*
* vectorGraph: *and a short explanation...*

## Report
The report is accessible from this url (be carefull, it allows r/w) : [Vector Racer Report](https://www.writelatex.com/1093436xbwmmw)
