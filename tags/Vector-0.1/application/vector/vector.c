#include <vectorMap/read_map.h>
#include <vectorSDL/vector_sdl.h>

int main(int argc, char ** argv)
{
	if (argc == 2)
	{
		struct map * map = vector_map_load(argv[1]);
		struct sdl * sdl = vector_SDL_init(map);
		vector_SDL_print(map, sdl);
		SDL_Event event;
		int continuer = 1;
		while(continuer)
		{
		  SDL_WaitEvent(&event);
		  switch(event.type)
		  {
		  case SDL_QUIT:
		    continuer = 0;
		    break;
		  default:
		    break;
		  }
		}
		vector_map_print(map);
		vector_map_free(map);
	}
	return 0;
}