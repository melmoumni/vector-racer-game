#include <vectorMap/read_map.h>
#include <vectorSDL/vector_sdl.h>
#define CASE 25

struct sdl* vector_SDL_init(struct map* map)
{
  
  SDL_Init(SDL_INIT_VIDEO);
  
  SDL_Surface *screen = NULL;
  screen = SDL_SetVideoMode(CASE*map->w, CASE*map->h, 32, SDL_HWSURFACE|SDL_DOUBLEBUF);
  SDL_WM_SetCaption("Vector Race", NULL);
  SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 255, 255, 255));
  
  SDL_Surface *wall = NULL;
  wall = SDL_CreateRGBSurface(SDL_HWSURFACE, CASE-1 , CASE-1 , 32, 0, 0, 0, 0);
  SDL_FillRect(wall, NULL, SDL_MapRGB(wall->format, 2, 95, 22));
  
  SDL_Surface *road = NULL;
  road = SDL_CreateRGBSurface(SDL_HWSURFACE, CASE-1 , CASE-1 , 32, 0, 0, 0, 0);
  SDL_FillRect(road, NULL, SDL_MapRGB(road->format, 125, 125, 125));
  

  struct sdl* sdl = malloc(sizeof(struct sdl));
  sdl->screen = screen;
  sdl->wall = wall;
  sdl->road = road;
  
  return sdl;
  
}

int vector_SDL_print(struct map* map, struct sdl* sdl)
{
  
  SDL_Rect position = {0, 0, 0, 0};
  int i = 0;
  int j = 0;
  SDL_FillRect(sdl->screen, NULL, SDL_MapRGB(sdl->screen->format, 255, 255, 255));
  for (i = 0; i < map->h; i++)
    {
      position.x = CASE*j+1;
      position.y = CASE*i+1;
      for (j = 0; j < map->w; j++)
	{
	  position.x = CASE*j+1;
	  position.y = CASE*i+1;
	  if (map->grid[i][j] == '#')
	    SDL_BlitSurface(sdl->wall, NULL, sdl->screen, &position);
	  else
	    SDL_BlitSurface(sdl->road, NULL, sdl->screen, &position);
	}
      j = 0;
    }
  
  
  
  SDL_Flip(sdl->screen);
  
  return 0;
  
}

int vector_SDL_free(struct sdl* sdl)
{
  
  SDL_FreeSurface(sdl->wall);
  SDL_FreeSurface(sdl->screen);
  
  free(sdl);
  
  SDL_Quit();
  
  return 0;
}
