/*
USE gcc -std=c99 -Wall graph.c `pkg-config --cflags --libs glib-2.0`
-o graph  to compile
*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


#include "vectorGraph/graph.h"
#include "link.h"

#define MAXVERTEX 2000
#define EMPTY_LIST NULL


/***** Structure definition ******/

struct link;


struct vertex
{
  int vertexID;
  char *tag;
};

struct graph
{
  struct link **neighbors; //Adjacency list
  vertex* vertex[MAXVERTEX];    //!<Vertices of the graph
  int number_edges;
  int number_vertices;
};
/******************** Graph Functions **********************/

graph* graph_empty()
{
	graph* g = malloc(sizeof(graph));
	g->number_vertices = 0;
	g->number_edges = 0;
	return g;
}

//Design note: the vertex is appended to the array of vertexs: vertexID = nbVertices-1
vertex* graph_add_vertex(graph* g)
{
	assert(g != NULL);
	assert(g->number_vertices < MAXVERTEX);
	//Initialize the new vertex (adjacency,id and tag)
	g->neighbors[g->number_vertices] = EMPTY_LIST;
	g->vertex[g->number_vertices] = malloc(sizeof(vertex));
	g->vertex[g->number_vertices]->vertexID = g->number_vertices;
	g->vertex[g->number_vertices]->tag = NULL;
	g->number_vertices++;
	//return the new vertex
	return g->vertex[g->number_vertices-1];
}

int vertex_get_id(const vertex* v)
{
	return v->vertexID;
}

vertex* graph_get_vertex_from_id(const graph* g, int id)
{
	return g->vertex[id];
}

int graph_add_edge(graph* g, vertex* v1, vertex* v2)
{
	assert(g != NULL);
	assert(v1 != NULL);
	assert(v2 != NULL);

	//Adds v2 to the neighbor list of v1 and v2 to the one of v1
	g->neighbors[v1->vertexID -1] = lnk__add_tail(g->neighbors[v1->vertexID]); 
	g->neighbors[v2->vertexID -1] = lnk__add_tail(g->neighbors[v2->vertexID]); 
	g->number_edges++;

	return EXIT_SUCCESS;
}

int graph_remove_edge(graph* g, vertex* v1, vertex* v2)
{
	//Adds v2 to the neighbor list of v1 and v2 to the one of v1

  g->neighbors[v1->vertexID -1] =lnk__remove_tail(g->neighbors[v1->vertexID],v2);
  g->neighbors[v2->vertexID -1] =lnk__remove_tail(g->neighbors[v1->vertexID], v2);
  return 0;
}


int graph_is_vert_in(const graph* g, const vertex* v)
{
	assert(g != NULL);
	assert(v != NULL);
	if(v->vertexID < g->number_vertices)
	{
		return v == g->vertex[v->vertexID];
	}
	else
		return EXIT_SUCCESS;
}


int graph_is_edge_in(const graph* g, const vertex* v1, const vertex* v2)
{
	assert(v1 != NULL);
	assert(v2 != NULL);
	assert(g != NULL);

	return (lnk__find(g->neighbors[v1->vertexID -1],v2->vertexID));
}

int graph_number_vertex(const graph* g)
{
	return g->number_vertices;
}

int graph_number_edges(const graph* g)
{
	return g->number_edges;
}

vertex_tag graph_get_vertex_tag(const graph* g, const vertex* v)
{
	assert(g != NULL);
	assert(v != NULL);
	return v->tag;
}

int graph_set_vertex_tag(graph* g, vertex* v, char tag)
{
	assert(g != NULL);
	assert(v != NULL);
	v->tag = tag;
	return EXIT_SUCCESS;
}

void graph_free(graph* g)
{
	assert(g != NULL);
	int i;
	//Free every vertex and every adjacency list
	for(i = 0; i < g->number_vertices; i++)
	{
		free(g->vertex[i]);
		lnk__free(g->neighbors[i]);
	}
	free(g);
}



//This part still uses Glib, DO NOT READ

/* //!\brief auxilliary funtion for graph_connex_dfs */
/* //!\parameter[in] color defines the state of every vertex (visited or not) */
/* static int gcdfs_aux(graph* g, vertex* u, vertex_action f, void* data, int color[]) */
/* { */
/* 	assert(u != NULL); */
/* 	color[u->vertexID] = 1; */
/* 	int action = f(g, u, data); //apply f to the vertex */
/* 	//if f asks to stop, stop */
/* 	if(action == DFS_STOP) */
/* 	{ */
/* 		return DFS_STOP; */
/* 	} */
/* 	//if f asks to skip the vertex set it as visited and continue */
/* 	else */
/* 		if(action == DFS_SKIPVERTEX) */
/* 		{ */
/* 			color[u->vertexID] = 2; */
/* 			return DFS_CONTINUE; */
/* 		} */
/* 	//if f asks to continue */
/* 	GSList* list; */
/* 	//For every neighbors of u */
/* 	for(list = g->neighbors[u->vertexID]; */
/* 	    list != NULL; */
/* 	    list = g_slist_next(list)) */
/* 	{ */
/* 		vertex* v = (vertex*)(list->data); */
/* 		if(color[v->vertexID] == 0) */
/* 			//if it was never visited, visit it */
/* 		{ */
/* 			action = gcdfs_aux(g, v, f, data, color); */
/* 			if(action == DFS_STOP) */
/* 				return DFS_STOP; */
/* 		} */
/* 	} */
/* 	//Set the vertex as visited; */
/* 	color[u->vertexID] = 2; */
/* 	return action; */
/* } */

/* int graph_connex_dfs(graph* g, vertex* v, vertex_action f, void* data) */
/* { */
/* 	//Set every vertex as unvisited */
/* 	int* color = (int*)calloc(graph_number_vertex(g), */
/* 	                          sizeof(int)); */
/* 	int res = gcdfs_aux(g, v, f, data, color); */
/* 	free(color); */
/* 	return res; */
/* } */
