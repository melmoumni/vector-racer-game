### SETUP LIBRARY ###
SET(LIB_NAME vectorSteps)
SET(HEADER_PATH ${Vector_SOURCE_DIR}/include/${LIB_NAME})
SET(TARGET_H
    ${HEADER_PATH}/read_steps.h
)
SET(TARGET_SRC
    read_steps.c
)

### REGISTER ###
ADD_LIBRARY(${LIB_NAME} ${TARGET_SRC} ${TARGET_H})
