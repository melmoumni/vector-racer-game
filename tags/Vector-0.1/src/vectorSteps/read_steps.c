#include "vectorSteps/read_steps.h" 

#include "vectorSteps/read_steps.h" 




//---------------------------------Enumerations-----------------------------------//
enum Q {start,q1,q2,q3,q4,err,end};
enum ERROR {er1,er2,er3,er4,er5,er6,er7};
//--------------------------------------------------------------------------------//


//---------------------------------Signatures-------------------------------------//
int steps_get_accel(FILE *file, char * file_name, struct steps *steps);
int steps_get_start(FILE *file, char * file_name, struct steps *steps);
int steps_get_steps(FILE *file, char * file_name, struct steps *steps);
int steps_get_accel_allowed(FILE *file, char *file_name,  struct steps *steps);
int steps_movements(FILE *file, char *file_name, struct steps *steps);
void print_error(enum ERROR error);
//---------------------------------------------------------------------------------//





int steps_get_accel(FILE *file, char * file_name, struct steps *steps)
{
  char str[LEN];
  char c;
  enum ERROR error;
  fgets(str,LEN,file);
  steps->num_accel = 0;
  if(strcmp(str,"Accel : ") != 0)
    error = er1;
  c = fgetc(file);
  if(isdigit(c))
    {
      do
	{
	  steps->num_accel = (steps->num_accel)*10 + c-'0';
	  c = fgetc(file);
	}
      while(isdigit(c));
      if((c == '\n' || c == EOF) && steps->num_accel != 0)
	return 1;
      else
	{
	  error = er1;
	  print_error(error);
	}
    }
  else
    {
      error = er1;
      print_error(error);
    }
  return 0;
}


int steps_get_start(FILE *file, char * file_name, struct steps *steps)
{
  char str[LEN];
  enum ERROR error;
  fgets(str,LEN,file);
  if(strcmp(str,"Start : ") != 0)
    error = er5;
  if(fgetc(file) != '\n')
    {
      steps->starting_position = (struct couple *)malloc(sizeof(struct couple));
      fseek(file,-1,SEEK_CUR);
      if((fscanf(file,"%d",&steps->starting_position->x) == 1) && (fgetc(file) == ',') && (fscanf(file,"%d",&steps->starting_position->y) == 1) && (fgetc(file) == '\n'))
	return 1;
      else
	{ 
	  error = er6;
	  print_error(error);
	}
    }
  else
    {
      error = er6;
      print_error(error);
    }
  free(steps->accel_allowed);
  free(steps->starting_position);
  return 0;
}


int steps_get_steps(FILE *file, char * file_name, struct steps *steps)
{
  char str[LEN];
  char c;
  enum ERROR error;
  fgets(str,LEN,file);
  steps->num_steps = 0;
  if(strcmp(str,"Steps : ") != 0)
    error = er2;
  c = fgetc(file);
  if(isdigit(c))
    {
      do
	{
	  steps->num_steps = steps->num_steps*10 + c-'0';
	  c = fgetc(file);
	}
      while(isdigit(c));
      if((c == '\n' || c == EOF) && steps->num_steps != 0)
	return 1;
      else
	{
	  error = er2;
	  print_error(error);
	}
    }
  else
    {
      error = er2;
      print_error(error);
    }
  free(steps->accel_allowed);
  free(steps->starting_position);
  return 0;
}

int steps_get_accel_allowed(FILE *file, char *file_name, struct steps *steps)
{
  int cmp = 0;
  char c = fgetc(file);
  enum ERROR error;
  int *tmp = NULL;
  steps->accel_allowed = (int *)malloc(2*steps->num_accel*sizeof(int));
  tmp = steps->accel_allowed;
  while(c != 'S' && c != EOF && cmp < steps->num_accel)
    {
      fseek(file,-1,SEEK_CUR);
      if((fscanf(file,"%d",tmp) == 1) && (fgetc(file) == ',') && (fscanf(file,"%d",++tmp) == 1) && (fgetc(file) == '\n'))
	{
	  c = fgetc(file);
	  cmp ++;
	  ++tmp;
	}
      else
	{
	  error = er3;
	  print_error(error);
	  free(steps->accel_allowed);
	  return 0;
	}
      
    }
  if(c == 'S'&& cmp == steps->num_accel)
    {
      fseek(file,-1,SEEK_CUR);
      return 1;
    }
  else
    {
      error = er4;
      print_error(error);
      free(steps->accel_allowed);
      return 0;
    }
}

int steps_get_movements(FILE *file, char *file_name, struct steps *steps)
{
  int cmp = 0;
  char c = fgetc(file);
  enum ERROR error;
  int *tmp = NULL;
  steps->movements = (int *)malloc(2*steps->num_steps*sizeof(int));
  tmp = steps->movements;
  while(c != EOF && cmp < steps->num_steps)
    {
      fseek(file,-1,SEEK_CUR);
      if((fscanf(file,"%d",tmp) == 1) && (fgetc(file) == ',') && (fscanf(file,"%d",++tmp) == 1) && (fgetc(file) == '\n'))
	{
	  c = fgetc(file);
	  cmp ++;
	  ++tmp;
	}
      else
	{
	  error = er3;
	  print_error(error);
	  free(steps->accel_allowed);
	  free(steps->starting_position);
	  free(steps->movements);
	  return 0;
	}
    }  
  if(c == EOF && cmp == steps->num_steps)
    {
      fseek(file,-1,SEEK_CUR);
      return 1;
    }
  else
    {
      error = er5;
      print_error(error);
      free(steps->accel_allowed);
      free(steps->starting_position);
      free(steps->movements);
      return 0;
    }
}

void print_error(enum ERROR error)
{
  switch(error){
  case er1:
    printf("Invalid accel definition. Definition must take this format: Accel : number\n");
    break;
  case er2:
    printf("Invalid steps definition. Definition must take this format: Steps : number.\n");
    break;
  case er3:
    printf("Invalid couple definition. Definitions must take this format: number,number\n");
    break;
  case er4:
    printf("Number of couples must be equal to number of accelerations\n");
    break;
  case er5:
    printf("Number of couples must be equal to number of steps\n");
    break;
  case er6:
    printf("Invalid start definition. Definition must take this format: Start : number,number.\n");
    break;
  case er7:
    printf("Correct file !\n");
    break;
  }
}



//function to free steps structure
void free_steps(struct steps *steps)
{
  free(steps->starting_position);
  free(steps->accel_allowed);
  free(steps->movements);
}

struct steps *steps_empty()
{
  struct steps *steps = (struct steps *)malloc(sizeof(struct steps));
  steps->starting_position = NULL;
  steps->accel_allowed = NULL;
  steps->movements = NULL;
  return steps;
	  
}
//main reading function represents reading automata
struct steps *steps_read(char *f)
{
  struct steps * steps = steps_empty();
  enum Q q = start;
  FILE *file = fopen(f,"r");
  int tmp = 0;
  if(file != NULL)
    {
      while (q != end && q != err)
	{
	  switch (q) {
	  case start:
	    if((tmp = steps_get_accel(file,f,steps)))
	      q = q1;
	    else
	      q = err;
	    break;
	  case q1:
	    if(steps_get_accel_allowed(file,f,steps))
	      q = q2;
	    else
	      q = err;
	    break;
	  case q2:
	    if(steps_get_start(file,f,steps))
	      q = q3;
	    else
	      q = err;
	    break;
	  case q3:
	    if(steps_get_steps(file,f,steps))
	      q = q4;
	    else
	      q = err;
	    break;
	  case q4:
	    if(steps_get_movements(file,f,steps))
	      q = end;
	    else
	      q = err;
	    break;
	  default:
	    break;
	  }
	}
      if(q == end)
	{
	  printf("Correct file !\n");
	  return steps;
	}
      return NULL;
    }
}

