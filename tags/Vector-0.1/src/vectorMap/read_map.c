#include <vectorMap/read_map.h>

struct map * vector_map_load(char *filename)
{
	errno = 0;
	FILE* file = fopen(filename, "r");
	if (file == NULL)
	{
    	goto fopenError;
    }

   	struct map *p_map = NULL;
    if ((p_map = get_map_size(file)) == NULL)
    {
    	goto getMapSizeError;
    }

    if ((p_map->grid = malloc(p_map->h * sizeof(p_map->grid))) == NULL)
    {
    	goto mallocMapError;
    }

    int error = 0;
    for (int i = 0 ; i < p_map->h ; i++)
    {
    	if ((p_map->grid[i] = malloc(p_map->w * sizeof(char))) == NULL)
    		error = 1;
    }
    if (error)
    	goto mallocGridError;

    enum err err;
    if (err = map_parse(file, p_map))
    {
    	goto mapParsingError;
    }

    return p_map;

mapParsingError:
	switch (err)
	{
		case SIZE:
			fprintf(stderr, "Wrong file format (wrong size ?).\n");
			break;
		case UNKNOWN_CHAR:
			fprintf(stderr, "Wrong character encountered.\n");
			break;
	}

mallocGridError:
	for (int i = 0 ; i < p_map->h ; i++)
	{
    	free(p_map->grid[i]);
    }
    free(p_map->grid);

mallocMapError:
	free(p_map);

getMapSizeError:
	fclose(file);

fopenError:
	if (errno)
		perror(NULL);
	return NULL;
}

struct map * get_map_size(FILE* file)
{
	while(fgetc(file) == '-')
		while(fgetc(file) != '\n' && fgetc(file) != EOF);

	fseek(file, -1, SEEK_CUR);

	errno = 0;
	struct map *p_map = malloc(sizeof(struct map));
	if (p_map == NULL)
	{
		return NULL;
	}

	if (fscanf(file, "%d %d\n", &(p_map->h), &(p_map->w)) != 2)
	{
		return NULL;
	}
	return p_map;
}

enum err map_parse(FILE* file, struct map * p_map)
{
	char c;
	int i = 0, j = 0;
	while((c = fgetc(file)) != EOF)
	{
		switch (c)
		{
			case '\n':
				i++;
				j = 0;
				break;
			case '-':
				while(fgetc(file) != '\n' && fgetc(file) != EOF);
				break;
			default:
				if (c == '#' || isdigit(c))
				{
					if (i >= p_map->h || j >= p_map->w)
						return SIZE;
					p_map->grid[i][j] = c;
					j++;
				}
				else
					return UNKNOWN_CHAR;
				break;
		}
	}
	return 0;
}

void vector_map_free(struct map * p_map)
{
	for (int i = 0; i < p_map->h; i++) {
    	free(p_map->grid[i]);
    }
    free(p_map);
}

void vector_map_print(struct map * p_map)
{
	for (int i = 0; i < p_map->h; i++)
	{
    	for (int j = 0; j < p_map->w; j++)
    	{
    		printf("%c", p_map->grid[i][j]);
    	}
    	printf("\n");
    }
}