#ifndef READ_FILE_H
#define READ_FILE_H
#define LEN 9

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>


struct couple
{
  int x;
  int y;
};


struct steps
{
  int num_accel;
  int num_steps;
  struct couple *starting_position;
  int *accel_allowed;
  int * movements;
};

struct steps * steps_read(char *f);
void steps_free(struct steps *steps);
void steps_print(struct steps *steps);
struct steps *steps_empty();

#endif
