#ifndef VECTOR_SDL
#define VECTOR_SDL

#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>

struct sdl
{
  SDL_Surface * screen;
  SDL_Surface * wall;
  SDL_Surface * road;
};

struct sdl* vector_SDL_init(struct map*);

int vector_SDL_print(struct map*,struct sdl*);

int vector_SDL_free(struct sdl*); 


#endif
