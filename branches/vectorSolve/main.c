#include "read_map.h"
#include "read_steps.h"
#include "dll.h"


int main(int argc, char **argv)
{
  struct map * map = vector_map_load(argv[1]);
  if(map == NULL)
    printf("hello\n");
  else
    {
      struct node  src,des;
      src.position = (struct couple *)malloc(sizeof(struct couple));
      src.current_speed = (struct couple *)malloc(sizeof(struct couple));
      des.position = (struct couple *)malloc(sizeof(struct couple));
      des.current_speed = (struct couple *)malloc(sizeof(struct couple));
      src.position->x = 4;
      src.position->y = 1;
      src.current_speed->x = 0;
      src.current_speed->y = 0;
      src.g = src.h = src.f = 0;
      src.parent = NULL;
      des.position->x = 4;
      des.position->y = 5;
      des.current_speed->x = 0;
      des.current_speed->y = 0;
      des.g = src.h = src.f = 0;
      des.parent = NULL;
      find_path(map,&src,&des);
    }
  return 0;
}
