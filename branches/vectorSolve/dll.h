#ifndef DLL_H
#define DLL_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "read_map.h"
struct lelement 
{
    struct node * data;
    struct lelement * prev;
    struct lelement * next;
};


struct list
{
  struct lelement * head;
  struct lelement * tail;
  int count;
};

struct list * list_empty();
int list_is_empty(struct list * list);
//tail
void list_add(struct list * list, struct node * value);
void list_remove(struct list * list, struct node * value);
void list_free(struct list * list);
void list_print(struct list * list);
struct list * list_concatenate(struct list * l1, struct list * l2);
#endif
