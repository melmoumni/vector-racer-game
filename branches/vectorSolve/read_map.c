#include "read_map.h"
#include "dll.h"



struct map * vector_map_load(char *filename)
{
        errno = 0;
        FILE* file = fopen(filename, "r");
        if (file == NULL)
        {
        goto fopenError;
    }

        struct map *p_map = NULL;
    if ((p_map = get_map_size(file)) == NULL)
    {
        goto getMapSizeError;
    }

    if ((p_map->grid = malloc(p_map->w*sizeof(p_map->grid))) == NULL)
    {
        goto mallocMapError;
    }

    char * p_grid;
    if ((p_grid = malloc(p_map->h * p_map->w * sizeof(p_map->grid))) == NULL)
    {
        goto mallocGridError;
    }

    for (int i = 0 ; i < p_map->w; i++)
    {
        p_map->grid[i] = &(p_grid[i*p_map->h]);
    }

    enum err err;
    if (err = map_parse(file, p_map))
    {
        goto mapParsingError;
    }

    return p_map;

mapParsingError:
        switch (err)
        {
                case SIZE:
                        fprintf(stderr, "Wrong file format (wrong size ?).\n");
                        break;
                case UNKNOWN_CHAR:
                        fprintf(stderr, "Wrong character encountered.\n");
                        break;
        }

mallocGridError:
        free(p_grid);

mallocMapError:
        free(p_map->grid);
        free(p_map);

getMapSizeError:
        fclose(file);

fopenError:
        if (errno)
                perror(NULL);
        return NULL;
}

struct map * get_map_size(FILE* file)
{
        while(fgetc(file) == '-')
                while(fgetc(file) != '\n' && fgetc(file) != EOF);

        fseek(file, -1, SEEK_CUR);

        errno = 0;
        struct map *p_map = malloc(sizeof(struct map));
        if (p_map == NULL)
        {
                return NULL;
        }

        if (fscanf(file, "%d %d\n", &(p_map->h), &(p_map->w)) != 2)
        {
                return NULL;
        }

        return p_map;
}

enum err map_parse(FILE* file, struct map * p_map)
{
        char c;
        int i = 0, j = 0;
        while((c = fgetc(file)) != EOF)
        {
                switch (c)
                {
                        case '\n':
                                j++;
                                i = 0;
                                break;
                        case '-':
                                while(fgetc(file) != '\n' && fgetc(file) != EOF);
                                break;
                        default:
                                if (c == '#' || isdigit(c) || c == '*')
                                {
                                        if (i >= p_map->w || j >= p_map->h)
                                                return SIZE;
                                        p_map->grid[i][p_map->h-j-1] = c;
                                        i++;
                                }
                                else
                                        return UNKNOWN_CHAR;
                                break;
                }
        }
        return 0;
}

void vector_map_free(struct map * p_map)
{
        free(p_map->grid);
    free(p_map);
}

void vector_map_print(struct map * p_map)
{
        for (int j = 0; j < p_map->h; j++)
        {
        for (int i = 0; i < p_map->w; i++)
        {
                printf("%c", p_map->grid[i][p_map->h - j - 1]);
        }
        printf("\n");
    }
}
int distance(struct couple * pos1, struct couple * pos2)
{
  int distance = 0;
  if(pos1->x > pos2->x)
    distance = distance + pos1->x - pos2->x;
  else
    distance = distance + pos2->x - pos2->x;
  if(pos1->y > pos2->y)
    distance = distance + (pos1->y - pos2->y);
  else
    distance = distance + (pos2->y - pos2->y);
  return distance;
}


//i>0
int look_for_zone(struct couple * map_zones, struct map * map, int i)
{
  int current_distance = 0;
  struct couple tmp;
  int tmp_distance;
  int bool = 0;
  for(int k = 0; k<map->w; k++)
    {
      for(int j = 0; j<map->h; j++)
	{
	  if((map->grid[k][j] - '0') == i)
	    {
	      tmp.x = k;
	      tmp.y = j;
	      if((tmp_distance = distance(&map_zones[i-1],&tmp))>=current_distance)
		{
		  map_zones[i].x = tmp.x;
		  map_zones[i].y = tmp.y;
		  current_distance = tmp_distance;
		}
	      bool = 1;
	    }	
	}
    }
  return bool; 
}


struct couple * vector_map_zones(struct map * map)
{
  struct couple * map_zones = (struct couple *)malloc(NUMBER_OF_ZONES*sizeof(struct couple));
  for(int i = 0; i<map->w; i ++)
    {
      for(int j = 0; j<map->h; j++)
	{
	  if(map->grid[i][j] == '*')
	    {
	      map_zones[0].x = i;
	      map_zones[0].y = j;
	    }
	}
    }
  int i = 1;
  while(look_for_zone(map_zones,map,i))
    i++;
  for(int j = i; j<NUMBER_OF_ZONES; j++)
    {
      map_zones[j].x = -1;
      map_zones[j].y = -1;
    }    
  return map_zones;
}


int number_of_zones(struct couple * map_zones)
{
  int i = 0;
  while(map_zones[i].x > 0 && map_zones[i].y > 0)
    i++;
  return i+1;
}



void print_map_zones(struct couple * map_zones)
{
  for(int i = 0; i<NUMBER_OF_ZONES;i++)
    {
      printf("zone %d : (%d,%d)\n",i+1,map_zones[i].x,map_zones[i].y);
    }
}


struct node * least_f(struct list * list)
{
  int lowest_f = list->head->data->f;
  struct node * q = list->head->data;
  for(struct lelement * tmp = list->head; tmp != NULL; tmp = tmp->next)
    {
      if(tmp->data->f < lowest_f)
	{
	  lowest_f = tmp->data->f;
	  q = tmp->data;
	}
    }
  return q;
}

struct list * generate_successors(struct map * map, struct node * q)
{
  struct list * successors = list_empty();
  if(isdigit(map->grid[q->position->x + q->current_speed->x][q->position->y + q->current_speed->y]))
    {
      struct node * oo = (struct node *)malloc(sizeof(struct node));
      oo->position = (struct couple *)malloc(sizeof(struct couple));
      oo->current_speed = (struct couple *)malloc(sizeof(struct couple));
      oo->position->x = q->position->x + q->current_speed->x;
      oo->position->y = q->position->y + q->current_speed->y;
      oo->current_speed->x = q->current_speed->x;
      oo->current_speed->y = q->current_speed->y;
      oo->g = oo->h = oo->f = 0;
      oo->parent = q;
      list_add(successors,oo);
    }
  if(isdigit(map->grid[q->position->x + q->current_speed->x + 1][q->position->y + q->current_speed->y]))
    {
      struct node * ao = (struct node *)malloc(sizeof(struct node));
      ao->position = (struct couple *)malloc(sizeof(struct couple));
      ao->current_speed = (struct couple *)malloc(sizeof(struct couple));
      ao->position->x = q->position->x + q->current_speed->x + 1;
      ao->position->y = q->position->y + q->current_speed->y;
      ao->current_speed->x = q->current_speed->x + 1;
      ao->current_speed->y = q->current_speed->y;
      ao->g = ao->h = ao->f = 0;
      ao->parent = q;
      list_add(successors,ao);
    }
  if(isdigit(map->grid[q->position->x + q->current_speed->x - 1][q->position->y + q->current_speed->y]))
    {
      struct node * bo = (struct node *)malloc(sizeof(struct node));
      bo->position = (struct couple *)malloc(sizeof(struct couple));
      bo->current_speed = (struct couple *)malloc(sizeof(struct couple));
      bo->position->x = q->position->x + q->current_speed->x -1;
      bo->position->y = q->position->y + q->current_speed->y;
      bo->current_speed->x = q->current_speed->x - 1;
      bo->current_speed->y = q->current_speed->y;
      bo->g = bo->h = bo->f = 0;
      bo->parent = q;
      list_add(successors,bo);
    }
  if(isdigit(map->grid[q->position->x + q->current_speed->x][q->position->y + q->current_speed->y + 1]))
    {
      struct node * oa = (struct node *)malloc(sizeof(struct node));
      oa->position = (struct couple *)malloc(sizeof(struct couple));
      oa->current_speed = (struct couple *)malloc(sizeof(struct couple));
      oa->position->x = q->position->x + q->current_speed->x;
      oa->position->y = q->position->y + q->current_speed->y + 1;
      oa->current_speed->x = q->current_speed->x;
      oa->current_speed->y = q->current_speed->y + 1;
      oa->g = oa->h = oa->f = 0;
      oa->parent = q;
      list_add(successors,oa);
    }
 if(isdigit(map->grid[q->position->x + q->current_speed->x][q->position->y + q->current_speed->y - 1]))
    {
      struct node * ob = (struct node *)malloc(sizeof(struct node));
      ob->position = (struct couple *)malloc(sizeof(struct couple));
      ob->current_speed = (struct couple *)malloc(sizeof(struct couple));
      ob->position->x = q->position->x + q->current_speed->x;
      ob->position->y = q->position->y + q->current_speed->y - 1;
      ob->current_speed->x = q->current_speed->x;
      ob->current_speed->y = q->current_speed->y - 1;
      ob->g = ob->h = ob->f = 0;
      ob->parent = q;
      list_add(successors,ob);
    }
 if(isdigit(map->grid[q->position->x + q->current_speed->x + 1][q->position->y + q->current_speed->y + 1]))
    {
      struct node * aa = (struct node *)malloc(sizeof(struct node));
      aa->position = (struct couple *)malloc(sizeof(struct couple));
      aa->current_speed = (struct couple *)malloc(sizeof(struct couple));
      aa->position->x = q->position->x + q->current_speed->x + 1;
      aa->position->y = q->position->y + q->current_speed->y + 1;
      aa->current_speed->x = q->current_speed->x + 1;
      aa->current_speed->y = q->current_speed->y + 1;
      aa->g = aa->h = aa->f = 0;
      aa->parent = q;
      list_add(successors,aa);
    }
 if(isdigit(map->grid[q->position->x + q->current_speed->x - 1][q->position->y + q->current_speed->y - 1]))
    {
      struct node * bb = (struct node *)malloc(sizeof(struct node));
      bb->position = (struct couple *)malloc(sizeof(struct couple));
      bb->current_speed = (struct couple *)malloc(sizeof(struct couple));
      bb->position->x = q->position->x + q->current_speed->x - 1;
      bb->position->y = q->position->y + q->current_speed->y - 1;
      bb->current_speed->x = q->current_speed->x - 1;
      bb->current_speed->y = q->current_speed->y - 1;
      bb->g = bb->h = bb->f = 0;
      bb->parent = q;
      list_add(successors,bb);
    }
 if(isdigit(map->grid[q->position->x + q->current_speed->x + 1][q->position->y + q->current_speed->y - 1]))
    {
      struct node * ab = (struct node *)malloc(sizeof(struct node));
      ab->position = (struct couple *)malloc(sizeof(struct couple));
      ab->current_speed = (struct couple *)malloc(sizeof(struct couple));
      ab->position->x = q->position->x + q->current_speed->x + 1;
      ab->position->y = q->position->y + q->current_speed->y - 1;
      ab->current_speed->x = q->current_speed->x + 1;
      ab->current_speed->y = q->current_speed->y - 1;
      ab->g = ab->h = ab->f = 0;
      ab->parent = q;
      list_add(successors,ab);
    }
 if(isdigit(map->grid[q->position->x + q->current_speed->x - 1][q->position->y + q->current_speed->y + 1]))
    {
      struct node * ba = (struct node *)malloc(sizeof(struct node));
      ba->position = (struct couple *)malloc(sizeof(struct couple));
      ba->current_speed = (struct couple *)malloc(sizeof(struct couple));
      ba->position->x = q->position->x + q->current_speed->x - 1;
      ba->position->y = q->position->y + q->current_speed->y + 1;
      ba->current_speed->x = q->current_speed->x - 1;
      ba->current_speed->y = q->current_speed->y + 1;
      ba->g = ba->h = ba->f = 0;
      ba->parent = q;
      list_add(successors,ba);
    }
 return successors; 
}
int same_position_lower_f(struct list * list, struct node *q)
{
  for(struct lelement * tmp = list->head; tmp != NULL; tmp = tmp->next)
    {
      if((tmp->data->position->x == q->position->x) && (tmp->data->position->y == q->position->y) && (tmp->data->f < q->f))
	return 1;
    } 
  return 0;
}



void find_path(struct map * map, struct node * src, struct node * des)
{
  struct list * open_list = list_empty();
  struct list * closed_list = list_empty();
  struct list * successors;
  struct node * q = NULL;
  int i = 1, j = 1; 
  list_add(open_list,src);
  while(list_is_empty(open_list) == 0)
    {
      printf(" i = %d\n",i);
      i++;
      j = 1;
      q = least_f(open_list);
      printf("least f \n");
      list_remove(open_list,q);
      printf("removed\n");
      successors = generate_successors(map,q);
      printf("generated\n");
      for(struct lelement * tmp = successors->head; tmp != NULL; tmp = tmp->next)
	{
	  printf("j = %d\n",j);
	  j++;
	  if(tmp->data == des)
	    {
	      break;
	    }
	  else
	    {
	      tmp->data->g = q->g + distance(tmp->data->position,q->position);
	      tmp->data->h = distance(tmp->data->position, des->position);
	      tmp->data->f = tmp->data->g + tmp->data->h;
	      if(!same_position_lower_f(open_list,tmp->data) && !same_position_lower_f(closed_list,tmp->data))
		{
		  list_add(open_list,tmp->data);
		  printf("added to open\n");
		}
	    }
	}
      list_add(closed_list,q);
      printf("added to closed\n");
    }
}




/* struct list * vector_map_solution(struct map * map) */
/* { */
/*   struct couple * map_zones = vector_map_zones(map); */
/*   int n = number_of_zones(map_zones); */
/*   int i = 0; */
/*   struct list * list = list_empty(); */
/*   struct node * src = (struct node *)malloc(sizeof(struct node)); */
/*   src->position.x = map_zones[0].x; */
/*   src->position.y = map_zones[0].y; */
/*   src->current_speed.x = src->current_speed.x = 0; */
/*   src->g = 0; */
/*   src->h = 0; */
/*   src->f = 0; */
/*   src->parent = NULL; */
/*   while(i < n-1) */
/*     { */
/*       struct node * des = (struct node *)malloc(sizeof(struct node)); */
/*       src->position.x = map_zones[i+1].x; */
/*       src->position.y = map_zones[i+1].y; */
/*       src->current_speed.x = src->current_speed.x = 0; */
/*       src->g = 0; */
/*       src->h = 0; */
/*       src->f = 0; */
/*       src->parent = NULL; */
/*       list = list_concatenate(list,find_path(map,src,des)); */
/*       src = list->tail->data; */
/*       i++; */
/*     } */
/*   return list; */
/* } */



