#ifndef READ_FILE_H
#define READ_FILE_H
#define LEN 9
#define NUMBER_OF_ZONES 10
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>

struct couple
{
  int x;
  int y;
};


struct steps
{
  int num_accel;
  int num_steps;
  struct couple *starting_position;
  struct couple *accel_allowed;
  struct couple * movements;
};
//ERRORS
enum ERROR {er1,er2,er3,er4,er5,er6,er7,noer};

struct steps *steps_empty();
//main reading function
struct steps *steps_load(char *f);
enum ERROR steps_parse(char *f, struct steps *steps);
//function to free steps structure
void steps_free(struct steps *steps);
void steps_print(struct steps *steps);
int steps_verify(struct steps *steps);
int steps_is_solution(char *fs, char *fm);
#endif
