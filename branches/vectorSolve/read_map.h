#ifndef MAP_UTILS_H_INCLUDED
#define MAP_UTILS_H_INCLUDED
#define color(param) printf("\033[%sm",param)
#define LOGLEVEL 1

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include "read_steps.h"
#include "dll.h"

struct map
{
	int h;
	int w;
	char** grid;
};

struct point
{
	int x;
	int y;
};

enum err
{
	UNKNOWN_CHAR = 1, SIZE
};

struct map * vector_map_load(char *);
struct map * get_map_size(FILE *);
enum err map_parse(FILE *, struct map *);
void vector_map_free(struct map *);
void vector_map_print(struct map * p_map);


struct node
{
  struct couple * position;
  struct couple * current_speed;
  int g;
  int h;
  int f;
  int is_obstacle;
  struct node * parent;
};


struct map * vector_map_load(char *);
struct map * get_map_size(FILE *);
enum err map_parse(FILE *, struct map *);
void vector_map_free(struct map *);
void vector_map_print(struct map * p_map);
struct couple * vector_map_zones(struct map * map);
void print_map_zones(struct couple * map_zones);
struct list * vector_map_solution(struct map * map);
void find_path(struct map * map, struct node * src, struct node * des);
#endif
