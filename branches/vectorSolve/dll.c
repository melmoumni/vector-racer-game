#include "dll.h"


struct list * list_empty()
{
  struct list * list = (struct list *)malloc(sizeof(struct list));
  list->head = list->tail = NULL;
  list->count = 0;
  return list;
}

int list_is_empty(struct list * list)
{
  if(list->head == NULL && list->tail == NULL)
    return 1;
  return 0;
}



void list_add(struct list * list, struct node * value)
{
  struct lelement * elmt = (struct lelement *)malloc(sizeof(struct lelement));
  elmt->data = value;
  if(list_is_empty(list))
    {
      list->head = elmt;
      list->tail = elmt;
      elmt->prev = NULL;
      elmt ->next = NULL;
    }
  else
    {
      list->tail->next = elmt;
      elmt->prev = list->tail;
      list->tail = elmt;
      list->tail->next = NULL;
    }
}

void list_remove(struct list * list, struct node * value)
{
  struct lelement * elmt = list->head;
  while(elmt != NULL && elmt->data != value)
    {
      elmt = elmt->next;
    }
  if(elmt != NULL)
    {
      if(elmt = list->head)
	{
	  list->head = elmt->next;
	  //free(elmt);
	}
      else if(elmt == list->tail)
	{
	  list->tail = elmt->prev;
	  list->tail->next = NULL;
	  //free(elmt);
	}
      else
	{
	  elmt->prev->next = elmt->next;
	  elmt->next->prev = elmt->prev;
	  //free(elmt);
	}
    }
}

void list_print(struct list * list)
{
  struct lelement * tmp = list->head;
  while(tmp != NULL)
    {
      printf("%s\n",(char *)tmp->data);
      tmp = tmp->next;
    }
}

struct list * list_concatenate(struct list * l1, struct list * l2)
{
  l1->tail->next = l2->head;
  l2->head->prev = l1->tail;
  return l1;
}
